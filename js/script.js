//                                     ДР №4
//                                    Теория

// 1. Функция - важнейший элемент програмирования, позволяющий группировать и 
// обобщать програмный код, который может позднее использоваться произвольное число раз.

// 2. Аргумент функции - одно или несколько значений, которые передаются в функцию 
// для их обработки с помощью кода функции.

// 3. Оператор 'return' необходим в функции, что бы возвращать значение. 'return' возвращает
//  переменную 'result'. Его можно указвать явно, можно неявно через стрелочную функцию.

//                                     Практика
//                                        #1

let a;
let b;

do {
    a = Number(+prompt("Please input first number", ''));
} while (typeof a !== "number" || !a);

do {
    b = Number(+prompt("Please input second number", ''));
} while (typeof b !== "number" || !b);

let operation = prompt("Please choose the mathimatical operation (+, -, *, /)", '')

function calc(a, b, operation) {

    switch (operation) {
        case "+":
            return a + b;

        case "-":
            return a - b;

        case "*":
            return a * b;

        case "/":
            return a / b;
    }
}
console.log(calc(a, b, operation));


